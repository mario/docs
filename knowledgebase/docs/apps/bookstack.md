# <img src="/documentation/img/bookstack-logo.png" width="25px"> BookStack App

## Customization

Use the [Web terminal](/documentation/apps#web-terminal)
to edit custom configuration under `/app/data/env`.

See [BookStack customization docs](https://www.bookstackapp.com/docs/admin/visual-customisation/)
for more information.

## External registration

Bookstack does not allow external users to register when Cloudron user management (LDAP)
is enabled. If you require external registration, install Bookstack with Cloudron user
management disabled.

See the [Bookstack docs](https://www.bookstackapp.com/docs/admin/third-party-auth/) to
enable 3rd party auth like Google, GitHub, Twitter, Facebook & others.

