# <img src="/documentation/img/openvpn-logo.png" width="25px"> OpenVPN App

## Desktop and Mobile Clients

The OpenVPN app has been tested with the following clients:

* NetworkManager on Ubuntu
* [Tunnelblick](https://www.tunnelblick.net/) on Mac OS X
* [OpenVPN for Android](https://play.google.com/store/apps/details?id=de.blinkt.openvpn)

## How to connect on Ubuntu 16.04

* Install the Network Manager OpenVPN plugin
```
sudo apt-get install network-manager-openvpn-gnome
```

* Download the .ovpn embedded certs config file from the OpenVPN app

<center>
<img src="/documentation/img/openvpn-config.png" class="shadow">
</center>

* Open Network Manager, `VPN Settings` -> `Import from file...`

## DNS Server

This app has a built-in DNS server (which is pushed to clients). This DNS server
allows resolution of connected clients using `devicename.username`.

If you have an internal DNS server, you can configure Cloudron to forward all DNS
queries to the [internal DNS server](https://cloudron.io/documentation/networking/#internal-dns-server).
This way clients can resolve internal addresses as well.

## Customizations

You can customize various settings by editing `/app/data/openvpn.conf` using
the  [Web terminal](/documentation/apps#web-terminal). Some popular options are
discussed below:

### Network address

By default, the VPN uses the 10.8.0.0 network. If you happen to connect to multiple VPN
networks at the same time, this can cause an address conflict. To change the network to say
10.12.0.0, edit the file as below and restart the app.

```
server 10.12.0.0 255.255.255.0
push "dhcp-option DNS 10.12.0.1"
```

### Inter client connectivity

By default, the app allows clients to talk to each other. To disable this, edit the file as below
and restart the app.

```
# client-to-client
```

### Custom routes

By default, clients are configured to route all traffic via the VPN. If you disable this, you would
want to push custom routes for the network and hosts behind the VPN. For example, edit the file as below and
restart the app.

```
# push "redirect-gateway def1 bypass-dhcp"
push "route 178.128.183.220 255.255.255.255"
push "route 178.128.74.0 255.255.255.0"
```

## Privacy

The OpenVPN app provides a tunnel to channel all the traffic from your
devices via the Cloudron. Websites and services that you visit will
not see the IP address of your devices but they *will* see the IP
address and possibly the RDNS hostname of your Cloudron.

You can check what sort of information can be gathered from your
Cloudron's IP address using [ipleak.net](https://ipleak.net).

## Troubleshooting

If you are unable to connect to the OpenVPN server, make sure that your VPS firewall
allows the OpenVPN port (by default, this is 7494/TCP). For example, you might have
to add this incoming port as part of EC2 security group.

