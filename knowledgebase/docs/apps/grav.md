# <img src="/documentation/img/grav-logo.png" width="25px"> Grav App

## CLI

GPM and Grav commands can be executed by opening a [Web terminal](/documentation/apps#web-terminal):

```
# cd /app/code
# sudo -u www-data -- /app/code/bin/gpm install bootstrap4-open-matter
# sudo -u www-data -- /app/code/bin/grav
```

