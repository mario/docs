# <img src="/documentation/img/freescout-logo.png" width="25px"> FreeScout App

## Mailbox Setup

Mailboxes do not need to be hosted on Cloudron itself. The app acts as a regular email client and thus can be setup for any IMAP mailbox.
For sending emails of a specific mailbox, the STMP method has to be selected as `php mail()` or `sendmail` wont work on Cloudron.

<img src="/documentation/img/freescout-smtp-settings.png" class="shadow">
