# <img src="/documentation/img/kimai-logo.png" width="25px"> Kimai App

## Customization

Use the [Web terminal](/documentation/apps#web-terminal)
to edit custom configuration under `/app/data/local.yaml`.

See [Kimai customization docs](https://www.kimai.org/documentation/configurations.html)
for more information.

Once the `local.yaml` is updated, restart the app from the Cloudron dashboard.

## Troubleshooting

The `local.yaml` format is [YAML](https://lzone.de//cheat-sheet/YAML) and thus the format has to be valid,
otherwise the app would not start up. Especially check the indentation!

If after an update the Kimai instance is not starting, check the logs and revisit the values in the `/app/data/local.yaml` if they are still supported in the new version.
