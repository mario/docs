# <img src="/documentation/img/osticket-logo.png" width="25px"> osTicket App

## Admin Checklist

* Do not remove or change the email address of `osTicket Alerts` under 'Email Addresses'.
This mailbox is managed by Cloudron and you can change it from the email configuration of
the app in the Cloudron dashboard.

* Change the administrator email under `Emails` -> `Email Settings and Options`. If you miss this,
  osTicket will send alerts to this address and bounces get attached to tickets.

## User Management

osTicket is integrated with Cloudron user management. However, agents must be manually
added into osTicket before they can login. When adding an agent, choose LDAP as the
authentication backend.

<center>
<img src="/documentation/img/osticket-add-agent.png" class="shadow" width="500px">
</center>

