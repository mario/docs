# <img src="/documentation/img/emby-logo.png" width="25px"> Emby App

## Apps

Emby Apps for various devices can be downloaded [here](https://emby.media/download.html).

For iOS and Android, you should be able to connect simply using the `https://emby.mydomain.com`.
in the custom server url field. No further ports or custom api urls need to be specified.

