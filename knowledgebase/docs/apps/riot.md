# <img src="/documentation/img/riot-logo.png" width="25px"> Riot App

## Setup

Riot is a front end that lets you connect to Matrix home servers. See the [Synapse](/documentation/apps/synapse)
package for installing a home server.

This app is pre-configured to use the matrix installation at `matrix.yourdomain.com`.
For example, if you installed Riot at `riot.example.com`, the application is pre-configured
to use `matrix.example.com`.

You can change the homeserver location, by using a [Web Terminal](/documentation/apps/#web-terminal)
and editing `/app/data/config.json`.

## Custom configuration

You can add custom riot configuration using the
[Web terminal](/documentation/apps#web-terminal):

* Add any custom configuration in `/app/data/config.json`.
* Restart the app

See [config.json](https://github.com/vector-im/riot-web/blob/develop/docs/config.md) for
reference.

