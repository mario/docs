# <img src="/documentation/img/mastodon-logo.png" width="25px"> Mastodon App

## Admin

To make a user an administrator, use the [Web Terminal](https://cloudron.io/documentation/apps/#web-terminal)
and run the following command:

```
    bin/tootctl accounts modify <username> --role admin
```

## Adding users

When used with Cloudron authentication, simply add new users to the Cloudron dashboard.

Without Cloudron authentication, new users can be added using the CLI:

```
    bin/tootctl accounts create testusername --email=test@cloudron.io
```

## Federation Domain

Cloudron will setup Mastodon accounts to be of the form `username@social.example.org` when you install
Mastodon at `social.example.org`. We recommend using `social` as the subdomain for mastodon installations
since it's easy to remember and type.

Mastodon has a way to create accounts as `username@example.org` even when installed at `mastodon.example.org`.
The setup for this is complicated and in most cases unnecessary. Mastodon account names are not intended to
be remembered like usernames (it's not like email where you can start following another account). Instead,
users usually visit a website and click the 'Follow' button.

That said, you can change the account domain name by using the [Web Terminal](https://cloudron.io/documentation/apps/#web-terminal)
and changing `LOCAL_DOMAIN` in `/app/data/env.production`.

After that, you have to configure `LOCAL_DOMAIN`'s web server to serve up `.well-known/host-meta` query.
How you do this depends on where and how `LOCAL_DOMAIN` is hosted (since this can be an app outside Cloudron as well).

Here is a sample nginx configuration:

```
location = /.well-known/host-meta {
       return 301 https://social.example.org$request_uri;
}
```

You can also return an XML instead:

```
<?xml version="1.0" encoding="UTF-8"?>
<XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
  <Link rel="lrdd" type="application/xrd+xml" template="https://social.example.org/.well-known/webfinger?resource={uri}"/>
</XRD>
```

If the `LOCAL_DOMAIN` is an app on Cloudron, here are some hints:

* For WordPress, you can setup a redirect using [Redirection plugin](https://wordpress.org/plugins/redirection/)
* For Ghost,you can add a [redirects.json](https://ghost.org/tutorials/implementing-redirects/)
* For Surfer, simply upload the XML above into `.well-known/host-meta`.

## Following users

To follow external users, visit their mastodon account and click on 'Follow'. This will popup a window asking your
mastodon identity (which will be `username@LOCAL_DOMAIN`).

If you have an existing account on another server, you can bring those connections with you to your own server.
For this, go to Settings -> Data Export and download your following list as a CSV file, and finally
on your own server, you go to Settings -> Import and upload that file.

